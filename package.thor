require 'faker'
require 'erb'

class Package < Thor
  include Thor::Actions

  desc "push", "create a dummy package and push it to the given url"
  method_option :user, required: true, desc: 'GitLab username'
  method_option :token, required: true, desc: 'GitLab PAT token'
  method_option :url, required: true, desc: 'GitLab Package registry url to upload to'
  method_option :package_type, required: true, desc: 'Package type. Available types: nuget, npm, conan, maven, gradle, pypi'
  method_option :name, desc: 'Package name. Default to a random value.', default: Faker::App.name
  method_option :version, desc: 'Package Version. Default to a random value.', default: Faker::App.semantic_version
  method_option :npm_package_scope, desc: 'Package scope for npm'
  method_option :conan_project_path, desc: 'Project path (required for conan)'
  method_option :keep, type: :boolean, desc: 'Keep the `pkg` folder where the package files are located'
  def push
    in_pkg_folder do
      save_credentials
      create_package
      push_package
    ensure
      remove_credentials
    end
    say('All done.', :green)
  end

  private

  def in_pkg_folder
    FileUtils.rm_rf('pkg') if File.directory?('pkg')
    FileUtils.mkdir('pkg')
    FileUtils.chdir('pkg') { yield }
  ensure
    FileUtils.rm_rf('pkg') if File.directory?('pkg') && !options[:keep]
  end

  def save_credentials
    say("Creating credentials for #{options[:user]}", :green)

    case options[:package_type]
    when 'conan'
      system *%W(conan remote add gl_pru #{options[:url]})
      system *%W(conan user #{options[:user]} -r gl_pru -p #{options[:token]})
    end
  end

  def create_package
    say("Creating package name: #{options[:name]}, version: #{options[:version]}", :green)
    case options[:package_type]
    when 'nuget'
      system *%W(dotnet new console --name #{options[:name]})
      FileUtils.chdir(options[:name]) do
        template('nuget/nuget.config.erb', 'nuget.config')
        template('nuget/csproj.erb', "#{options[:name]}.csproj")
      end
    when 'maven'
      system *%W(mvn -B archetype:generate -DarchetypeGroupId=org.apache.maven.archetypes -DgroupId=gl.pru -DartifactId=#{options[:name]} -Dversion=#{options[:version]})
      FileUtils.chdir(options[:name]) do
        template('maven/pom.xml.erb', 'pom.xml')
        template('maven/settings.xml.erb', 'settings.xml')
      end
    when 'maven_plugin'
      system *%W(mvn -B archetype:generate -DarchetypeGroupId=org.apache.maven.archetypes -DarchetypeArtifactId=maven-archetype-plugin  -DgroupId=gl.pru -DartifactId=#{options[:name]} -Dversion=#{options[:version]})
      FileUtils.chdir(options[:name]) do
        template('maven_plugin/pom.xml.erb', 'pom.xml')
        template('maven/settings.xml.erb', 'settings.xml')
      end
    when 'gradle'
      FileUtils.mkdir(options[:name])
      FileUtils.chdir(options[:name]) do
        system *%W(gradle init --dsl groovy --project-name #{options[:name]} --test-framework junit --type java-application --package #{options[:name]})
        template('gradle/build.gradle.erb', 'build.gradle')
      end
    when 'npm'
      FileUtils.mkdir(options[:name])
      FileUtils.chdir(options[:name]) do
        template('npm/package.json.erb', 'package.json')
        template('npm/.npmrc.erb', '.npmrc')
      end
    when 'conan'
      FileUtils.mkdir(options[:name])
      FileUtils.chdir(options[:name]) do
        system *%W(conan new #{options[:name]}/#{options[:version]} -t)
        system *%W(conan create . #{conan_project_path}/beta)
      end
    when 'pypi'
      FileUtils.mkdir(options[:name])
      FileUtils.chdir(options[:name]) do
        template('pypi/main.erb', 'main')
        system *%W(chmod +x main)
        template('pypi/README.md', 'README.md')
        template('pypi/setup.py.erb', 'setup.py')
        system *%W(python3 setup.py sdist bdist_wheel)
      end
    when 'generic'
      FileUtils.mkdir(options[:name])
      FileUtils.chdir(options[:name]) do
        File.write('dummy.txt', Time.now.to_i)
      end
    end
    say("Package #{options[:name]}, version: #{options[:version]} created.", :green)
  end

  def push_package
    say("Uploading package #{options[:name]}, version: #{options[:version]} to #{options[:url]}.", :green)
    FileUtils.chdir(options[:name]) do
      case options[:package_type]
      when 'nuget'
        system *%W(dotnet pack --configuration Release)
        system *%W(dotnet nuget push bin/Release/#{options[:name]}.#{options[:version]}.nupkg --source gl_pru)
      when 'maven', 'maven_plugin'
        system *%W(mvn deploy -s settings.xml -DskipTests)
      when 'gradle'
        system *%W(gradle publish)
      when 'npm'
        system *%W(npm publish)
      when 'conan'
        system *%W(conan upload #{options[:name]}/#{options[:version]}@#{conan_project_path}/beta --all --remote=gl_pru )
      when 'pypi'
        system *%W(python3 -m twine upload --repository-url #{options[:url]} -u #{options[:user]} -p #{options[:token]} dist/*)
      when 'generic'
        system "curl --header \"PRIVATE-TOKEN: #{options[:token]}\" --upload-file ./dummy.txt \"#{options[:url]}/#{options[:name]}/#{options[:version]}/file.txt\""
      end
    end
    say("Upload done.", :green)
  end

  def remove_credentials
    say("Removing credentials for #{options[:user]}", :green)
    case options[:package_type]
    when 'conan'
      system *%W(conan user None -r gl_pru)
      system *%W(conan remote remove gl_pru)
    end
  end

  def template(source, destination)
    renderer = ::ERB.new(File.read(File.join(File.dirname(__FILE__), 'templates', source)))
    File.write(destination, renderer.result(binding))
  end

  def conan_project_path
    error('Please provide the --project-path parameter, see the README file for examples') unless options[:conan_project_path]

    options[:conan_project_path]
  end
end
