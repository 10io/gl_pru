# GitLab Packages Registry Utility

### What is this about?

This is a collection of [thor](https://github.com/erikhuda/thor) scripts to help me in my interactions with package managers CLI command interactions.

This also a blatant proof that my memory is limited and doesn't serve me well :)

### Installation

```shell
$ bundle install
```

and you're reading to go.

### Usage

#### CLI helper

```shell
Usage:
  thor cli_helper:set_proxy

Options:
  [--cli=CLI]              # Which CLI do you want to update? Allowed values: [npm]
  [--proxy-url=PROXY_URL]  # The proxy url

Set a proxy url on the given CLI
```

```shell
Usage:
  thor cli_helper:unset_proxy

Unset a proxy url on the given CLI
```

#### Package

```shell
Usage:
  thor package:push --package-type=PACKAGE_TYPE --token=TOKEN --url=URL --user=USER

Options:
  --user=USER                                # GitLab username
  --token=TOKEN                              # GitLab PAT token
  --url=URL                                  # GitLab Package registry url to upload to
  --package-type=PACKAGE_TYPE                # Package type. Available types: nuget, npm, conan, maven, gradle, pypi
  [--name=NAME]                              # Package name. Default to a random value.
                                             # Default: Hatity
  [--version=VERSION]                        # Package Version. Default to a random value.
                                             # Default: 3.9.9
  [--npm-package-scope=NPM_PACKAGE_SCOPE]    # Package scope for npm
  [--conan-project-path=CONAN_PROJECT_PATH]  # Project path (required for conan)
  [--keep], [--no-keep]                      # Keep the `pkg` folder where the package files are located

create a dummy package and push it to the given url
```

### Supported package types

#### Nuget

Requirements:
* `dotnet` installed

Examples:

```shell
bundle exec thor package:push --package-type=nuget --user=root --token=XXXX --url=https://gitlab.example.com/api/v4/projects/1/packages/nuget/index.json --name=bananas --version=1.3.7
```

#### Maven

Requirements:
* `mvn` installed
* `java` installed

Examples:

```shell
bundle exec thor package:push --package-type=maven --user=root --token=XXX --url=https://gitlab.example.com/api/v4/projects/1/packages/maven --name=bananas --version=1.3.7
```

#### Maven plugin

Requirements:
* `mvn` installed
* `java` installed

Examples:

```shell
bundle exec thor package:push --package-type=maven_plugin --user=root --token=XXX --url=https://gitlab.example.com/api/v4/projects/1/packages/maven --name=bananas --version=1.3.7
```

#### Gradle

Requirements:
* `gradle` installed
* `java` installed

Examples:

```shell
bundle exec thor package:push --package-type=gradle --user=root --token=XXX --url=https://gitlab.example.com/api/v4/projects/1/packages/maven --name=bananas --version=1.3.7
```

#### NPM

Requirements:
* `npm` installed

```shell
bundle exec thor package:push --package-type=npm --user=root --token=XXX --url=https://gitlab.example.com/api/v4/projects/1/packages/npm/ --name=bananas --version=1.3.7 --npm-package-scope=@group
```

CAUTION: the `npm-package-scope` parameter is mandatory!

#### Conan

Requirements:
* `conan` installed

```shell
bundle exec thor package:push --package-type=conan --user=root --token=XXX --url=https://gitlab.example.com/api/v4/packages/conan --name=bananas --version=1.3.7 --conan-project-path=group+project
```

CAUTION: the `conan-project-path` parameter is mandatory!

#### PyPi

Requires:
* `python3` installed
* `twine` python module installed
* `wheel` python module installed

```shell
bundle exec thor package:push --package-type=pypi --user=root --token=XXX --url=https://gitlab.example.com/api/v4/projects/1/packages/pypi --name=bananas --version=1.3.7
```

#### Generic

```shell
bundle exec thor package:push --package-type=generic --user=root --token=XXXX --url=https://gitlab.example.com/api/v4/projects/1/packages/generic --name=bananas --version=1.3.7
```

### TODOS

* Where are my tests?
* `system *W()` to make a system call? kek.
